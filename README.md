# Tasks
- Refactor code using Android/Object oriented programming best practices. Make it easier to read, maintain and test.
- Add unit tests
- Add possibility to filter characters by date
- Show character image and some additional information about character
- Handle network request state (loading, error etc.)
- When user click on character in the list, navigate to character details screen. Show image, some details about character and location. Location details: name, type, dimensions.
- Properly handle navigation


# Bonus tasks
- Add UI tests
- Handle configuration change e.g. screen rotation
- Under location details, add button to see whole list of residents (bonus). This button should navigate to separate screen with list of residents (names are enough).
- Add Deep Link to go directly to character page e.g., urban-android-homework:/character?id=100